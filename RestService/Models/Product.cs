﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Models
{
	public class Product
	{
		public string Code { get; set; }

		public string Description { get; set; }

		public string CategoryCode { get; set; }

		public double Price { get; set; }

		public double Vat { get; set; }

		public int Availability { get; set; }

		public int Arrivals { get; set; }

		public List<Option> Options { get; set; }

	}
}
