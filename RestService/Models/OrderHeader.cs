﻿using RestService.Directories.Ecommerce.Orders.Enums;
using System;
using System.Collections.Generic;

namespace RestService.Models
{
	public class OrderHeader
	{
		public long Id { get; set; }

		public OrderState State { get; set; }

		public int UserId { get; set; }

		public int ShippingAddressId { get; set; }

		public double TotalPrice { get; set; }

		public DateTime Date { get; set; }

		public string Note { get; set; }
	}
}
