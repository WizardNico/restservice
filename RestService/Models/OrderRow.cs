﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestService.Models
{
	public class OrderRow
	{
		public long Id { get; set; }

		public string ProductCode { get; set; }

		public int Quantity { get; set; }

		public double Price { get; set; }

		public DateTime CreationDate { get; set; }

		public DateTime UpdateDate { get; set; }

		public long OrderId { get; set; }

		public List<string> Options { get; set; }
	}
}
