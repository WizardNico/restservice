﻿using AgileMQ.DTO;
using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using RestService.Models;
using RestService.Utilities;
using System.Collections.Generic;
using System.Threading.Tasks;
using EcommerceOrdersModels = RestService.Directories.Ecommerce.Orders.Models;
using EcommerceOrdersEnums = RestService.Directories.Ecommerce.Orders.Enums;
using System;

namespace RestService.Controllers
{
	[Route("api/[controller]")]
	public class OrderController : Controller
	{
		private IAgileBus bus;

		public OrderController(IAgileBus bus)
		{
			this.bus = bus;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			EcommerceOrdersModels.OrderHeader header = new EcommerceOrdersModels.OrderHeader()
			{
				Id = id
			};
			header = await bus.GetAsync(header);
			OrderHeader result = Mapper.Map(header);

			return Ok(result);
		}

		[HttpGet]
		public async Task<IActionResult> GetPagedList(long pageIndex, int pageSize, double totalPrice)
		{
			Dictionary<string, object> filter = new Dictionary<string, object>
			{
				{ "state", EcommerceOrdersEnums.OrderState.Confirmed },
				{ "totalPrice", totalPrice }
			};
			Page<EcommerceOrdersModels.OrderHeader> page = await bus.GetPagedListAsync<EcommerceOrdersModels.OrderHeader>(filter, pageIndex, pageSize);

			List<OrderHeader> items = new List<OrderHeader>();
			foreach (EcommerceOrdersModels.OrderHeader order in page.Items)
			{
				items.Add(Mapper.Map(order));
			}

			return Ok(items);
		}

		[HttpPost]
		public async Task<IActionResult> Post(OrderHeader header)
		{
			EcommerceOrdersModels.OrderHeader msgHeader = new EcommerceOrdersModels.OrderHeader()
			{
				Note = header.Note,
				State = header.State,
				ShippingAddressId = header.ShippingAddressId,
				UserId = header.UserId
			};

			msgHeader = await bus.PostAsync(msgHeader);

			return Ok(msgHeader.Id);
		}

		[HttpPut]
		public async Task<IActionResult> Put(int id, OrderHeader header)
		{
			EcommerceOrdersModels.OrderHeader msgHeader = new EcommerceOrdersModels.OrderHeader()
			{
				Note = header.Note,
				State = header.State,
				ShippingAddressId = header.ShippingAddressId,
				UserId = header.UserId,
				Id = id,
			};

			await bus.PutAsync(msgHeader);
			return Ok();
		}

		[HttpDelete]
		public async Task<IActionResult> Delete(int id)
		{
			EcommerceOrdersModels.OrderHeader header = new EcommerceOrdersModels.OrderHeader()
			{
				Id = id
			};
			await bus.DeleteAsync(header);
			return Ok();
		}
	}
}