﻿using AgileMQ.DTO;
using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Directories.Ecommerce.Products.Shared;
using RestService.Models;
using RestService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcommerceProductsModels = RestService.Directories.Ecommerce.Products.Models;

namespace RestService.Controllers
{
	[Route("api/[controller]")]
	public class ProductController : Controller
	{
		private IAgileBus bus;

		public ProductController(IAgileBus bus)
		{
			this.bus = bus;
		}

		[HttpGet("{code}")]
		public async Task<IActionResult> Get(string code)
		{
			EcommerceProductsModels.Product product = new EcommerceProductsModels.Product()
			{
				Code = code
			};
			product = await bus.GetAsync(product);
			Product result = Mapper.Map(product);

			return Ok(result);
		}

		[HttpGet]
		public async Task<IActionResult> GetPagedList(long pageIndex, int pageSize, string categoryCode)
		{
			Dictionary<string, object> filter = new Dictionary<string, object>
			{
				{ "category", categoryCode }
			};
			Page<EcommerceProductsModels.Product> page = await bus.GetPagedListAsync<EcommerceProductsModels.Product>(filter, pageIndex, pageSize);

			List<Product> items = new List<Product>();
			foreach (EcommerceProductsModels.Product product in page.Items)
			{
				items.Add(Mapper.Map(product));
			}

			return Ok(items);
		}

		[HttpPut]
		public async Task<IActionResult> Put(Product product)
		{
			List<ProductOption> optionList = new List<ProductOption>();
			if (product.Options != null)
				product.Options.ForEach(opt => optionList.Add(new ProductOption() { Code = opt.Code }));

			EcommerceProductsModels.Product msgProduct = new EcommerceProductsModels.Product()
			{
				Code = product.Code,
				CategoryCode = product.CategoryCode,
				Description = product.Description,
				Vat = product.Vat,
				Price = product.Price,
				Movements = product.Arrivals,
				Options = optionList
			};

			await bus.PutAsync(msgProduct);

			return Ok();
		}

		[HttpPost]
		public async Task<IActionResult> Post(Product product)
		{
			List<ProductOption> optionList = new List<ProductOption>();
			if (product.Options != null)
				product.Options.ForEach(opt => optionList.Add(new ProductOption() { Code = opt.Code }));

			EcommerceProductsModels.Product msgProduct = new EcommerceProductsModels.Product()
			{
				Code = product.Code,
				CategoryCode = product.CategoryCode,
				Description = product.Description,
				Vat = product.Vat,
				Price = product.Price,
				Availability = product.Availability,
				Options = optionList
			};

			msgProduct = await bus.PostAsync(msgProduct);

			return Ok(msgProduct.Code);
		}

		[HttpDelete]
		public async Task<IActionResult> Delete(string code)
		{
			EcommerceProductsModels.Product product = new EcommerceProductsModels.Product()
			{
				Code = code
			};
			await bus.DeleteAsync(product);
			return Ok();
		}
	}
}
