﻿using AgileMQ.DTO;
using AgileMQ.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RestService.Models;
using RestService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EcommerceOrdersModels = RestService.Directories.Ecommerce.Orders.Models;

namespace RestService.Controllers
{
	[Route("api/[controller]")]
	public class OrderRowController : Controller
	{
		private IAgileBus bus;

		public OrderRowController(IAgileBus bus)
		{
			this.bus = bus;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			EcommerceOrdersModels.OrderRow row = new EcommerceOrdersModels.OrderRow()
			{
				Id = id
			};
			row = await bus.GetAsync(row);
			OrderRow result = Mapper.Map(row);
			return Ok(result);
		}

		[HttpGet]
		public async Task<IActionResult> GetList(long orderId)
		{
			Dictionary<string, object> filter = new Dictionary<string, object>
			{
				{ "orderId", orderId },
			};
			List<EcommerceOrdersModels.OrderRow> items = await bus.GetListAsync<EcommerceOrdersModels.OrderRow>(filter);

			List<OrderRow> list = new List<OrderRow>();
			foreach (EcommerceOrdersModels.OrderRow row in items)
			{
				list.Add(Mapper.Map(row));
			}

			return Ok(list);
		}

		[HttpPost]
		public async Task<IActionResult> Post(OrderRow row)
		{
			EcommerceOrdersModels.OrderRow msgRow = new EcommerceOrdersModels.OrderRow()
			{
				Quantity = row.Quantity,
				OrderId = row.OrderId,
				ProductCode = row.ProductCode,
				Options = row.Options
			};
			msgRow = await bus.PostAsync(msgRow);
			return Ok(msgRow.Id);
		}

		[HttpPut]
		public async Task<IActionResult> Put(OrderRow row)
		{
			EcommerceOrdersModels.OrderRow msgRow = new EcommerceOrdersModels.OrderRow()
			{
				Id = row.Id,
				Quantity = row.Quantity,
				ProductCode = row.ProductCode,
			};
			await bus.PutAsync(msgRow);
			return Ok();
		}

		[HttpDelete]
		public async Task<IActionResult> Delete(int id)
		{
			EcommerceOrdersModels.OrderRow msgRow = new EcommerceOrdersModels.OrderRow()
			{
				Id = id,
			};
			await bus.DeleteAsync(msgRow);
			return Ok();
		}
	}
}
