﻿using EcommerceOrdersModels = RestService.Directories.Ecommerce.Orders.Models;
using EcommerceOrdersEnums = RestService.Directories.Ecommerce.Orders.Enums;
using EcommerceProductsModels = RestService.Directories.Ecommerce.Products.Models;
using RestService.Models;
using System.Collections.Generic;
using System.Linq;

namespace RestService.Utilities
{
	public class Mapper
	{
		public static OrderHeader Map(EcommerceOrdersModels.OrderHeader orderHeader)
		{
			return new OrderHeader()
			{
				Id = orderHeader.Id.Value,
				Date = orderHeader.CreationDate.Value,
				Note = orderHeader.Note,
				TotalPrice = orderHeader.TotalPrice.Value,
				State = (EcommerceOrdersEnums.OrderState)orderHeader.State,
				UserId = orderHeader.UserId.Value,
				ShippingAddressId = orderHeader.ShippingAddressId.Value,
			};
		}

		public static OrderRow Map(EcommerceOrdersModels.OrderRow orderRow)
		{
			return new OrderRow()
			{
				Id = orderRow.Id.Value,
				CreationDate = orderRow.CreationDate.Value,
				Price = orderRow.Price.Value,
				Quantity = orderRow.Quantity.Value,
				UpdateDate = orderRow.UpdateDate.Value,
				ProductCode = orderRow.ProductCode,
				OrderId = orderRow.OrderId.Value,
				Options = orderRow.Options
			};
		}

		public static Product Map(EcommerceProductsModels.Product product)
		{
			List<Option> optionList = new List<Option>();
			if (product.Options != null)
				product.Options.ForEach(opt => optionList.Add(new Option() { AdditionalPrice = opt.AdditionalPrice.Value, Code = opt.Code, }));

			return new Product()
			{
				Description = product.Description,
				Code = product.Code,
				Availability = product.Availability.Value + product.Movements.Value,
				Price = product.Price.Value,
				Vat = product.Vat.Value,
				CategoryCode = product.CategoryCode,
				Options = optionList,
				Arrivals = product.Movements.Value
			};
		}
	}
}
