﻿using AgileMQ.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestService.Directories.Ecommerce.Orders.Models
{
	[QueuesConfig(Directory = "Ecommerce", Subdirectory = "Orders", ResponseEnabled = true)]
	public class OrderRow
	{
		[Required]
		public long? Id { get; set; }

		[Required]
		public string ProductCode { get; set; }

		[Required]
		[Range(1, 100)]
		public int? Quantity { get; set; }

		public double? Price { get; set; }

		public DateTime? CreationDate { get; set; }

		public DateTime? UpdateDate { get; set; }

		[Required]
		public long? OrderId { get; set; }

		public List<string> Options { get; set; }
	}
}
